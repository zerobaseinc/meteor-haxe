var expect = require('chai').expect;
var foo = 'bar';
var beverages = { tea: [ 'chai', 'matcha', 'oolong' ] };


describe('var `foo`', function () {
    it('is a string', function () {
        expect(foo).to.be.a('string');
    });

    it('is equal to`bar`', function () {
        expect(foo).to.equal('bar');
    });

    it('is a length of 3', function () {
        expect(foo).to.have.length(3);
    });
});

describe('var `beverages`', function () { 
    it('has an array of 3 elements, called `tea`', function () {
        expect(beverages).to.have.property('tea').with.length(3);
    });
    it('is to be tested somehow');
});