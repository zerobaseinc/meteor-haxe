## 動作要件

* nodejs
* npm
* grunt-cli
* meteor


リポジトリからクローン

	$ git clone https://zerobase@bitbucket.org/zerobase/meteor-unittest.git
	

## ディレクトリ構造について
	.
	├── app/ #Meteor app
	│   ├── client/
	│   ├── lib/
	│   ├── public/
	│   ├── server/
	│   ├── tests/
	│   └── smart.json
	│   
	├── haxe/
	│   ├── client/ # *.hx -> ../app/client/main.js
	│   └── server/ # *.hx -> ../app/server/main.js
	├── node_modules/
	├── build.hxml # Haxe build config
	├── Gruntfile.js
	├── package.json
	└── README.md


`app/`以下のディレクトリ構造はMeteorの[Structuring your application](http://docs.meteor.com/#structuringyourapp "Structuring your application")に従う

## Meteor

初期状態に`underscore`のみ追加

	$ cd app/
	$ meteor list --using
	autopublish
	insecure
	preserve-inputs
	underscore

## テスト

[Mocha](http://visionmedia.github.io/mocha/ "Mocha")と[Chai](http://chaijs.com/ "Chai")を利用

	
	# テストを実行
	$ grunt test
	
	# Haxeファイル(.hx)を監視して、変更があれば自動的に再ビルド
	$ grunt

## Haxeのビルド

    $ haxe build.hxml

## リンク

* [Documentation - Meteor](http://docs.meteor.com/ "Documentation - Meteor")
* [Mocha - the fun, simple, flexible JavaScript test framework](http://visionmedia.github.io/mocha/ "Mocha - the fun, simple, flexible JavaScript test framework")
* [Chai - BDD / TDD assertion library](http://chaijs.com/ "Chai - BDD / TDD assertion library")
* [Atmosphere](https://atmosphere.meteor.com/# "Atmosphere")
* Githubで参考にできそうなプロジェクト
	* [SachaG / Telescope](https://github.com/SachaG "SachaG / Telescope")
	* [DiscoverMeteor / Microscope](https://github.com/DiscoverMeteor "DiscoverMeteor / Microscope")	