'use strict';

module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-simple-mocha');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-haxe');

    grunt.initConfig({
        simplemocha: {
            all: { src: ['app/tests/**/*.js'] },

            options: {
                reporter: 'spec',
                slow: 200,
                timeout: 1000
            }
        },
        haxe: {
            compile: {
                hxml: 'build.hxml'
            }
        },
        watch: {
            all: {
                files: ['haxe/**/*.hx'],
                tasks: ['haxe:compile']
            }
        }
    });

    grunt.registerTask('test', ['simplemocha']);
    grunt.registerTask('compile', ['haxe:compile']);
    grunt.registerTask('default', ['watch']);

};