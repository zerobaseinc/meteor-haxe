package ;
import js.Lib;
import utils.Template;

class Hello
{
    private var template : Dynamic;
    
    public function new()
    {
        template = Template.getInstance();
        template.hello.events({
            'click input' : function () : Void {
                untyped __js__('console.log("you pressed the button");');
            }
        });
        template.hello.greeting = function () : String { return "Hello Haxe!"; };
    }
}